/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jlagosmu
 */
@Entity
@Table(name = "programacion")
@NamedQueries({
    @NamedQuery(name = "Programacion.findAll", query = "SELECT p FROM Programacion p")
    , @NamedQuery(name = "Programacion.findByIdProgramacion", query = "SELECT p FROM Programacion p WHERE p.idProgramacion = :idProgramacion")
    , @NamedQuery(name = "Programacion.findByConsecutivo", query = "SELECT p FROM Programacion p WHERE p.consecutivo = :consecutivo")
    , @NamedQuery(name = "Programacion.findByFechaEjecucion", query = "SELECT p FROM Programacion p WHERE p.fechaEjecucion = :fechaEjecucion")
    , @NamedQuery(name = "Programacion.findByFechaFinalizado", query = "SELECT p FROM Programacion p WHERE p.fechaFinalizado = :fechaFinalizado")
    , @NamedQuery(name = "Programacion.findByObservacion", query = "SELECT p FROM Programacion p WHERE p.observacion = :observacion")
    , @NamedQuery(name = "Programacion.findByReprogramacion", query = "SELECT p FROM Programacion p WHERE p.reprogramacion = :reprogramacion")})
public class Programacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Programacion")
    private Integer idProgramacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Consecutivo")
    private int consecutivo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha_Ejecucion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEjecucion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha_Finalizado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinalizado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Observacion")
    private String observacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "Reprogramacion")
    private String reprogramacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "programacionIdProgramacion")
    private List<Material> materialList;
    @JoinColumn(name = "Id_Cuenta", referencedColumnName = "id_cuenta")
    @ManyToOne(optional = false)
    private Cuenta idCuenta;
    @JoinColumn(name = "id_Act_tecnica", referencedColumnName = "id_Act_tec")
    @ManyToOne(optional = false)
    private ActividadTecnica idActtecnica;
    @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")
    @ManyToOne(optional = false)
    private Persona idPersona;

    public Programacion() {
    }

    public Programacion(Integer idProgramacion) {
        this.idProgramacion = idProgramacion;
    }

    public Programacion(Integer idProgramacion, int consecutivo, Date fechaEjecucion, Date fechaFinalizado, String observacion, String reprogramacion) {
        this.idProgramacion = idProgramacion;
        this.consecutivo = consecutivo;
        this.fechaEjecucion = fechaEjecucion;
        this.fechaFinalizado = fechaFinalizado;
        this.observacion = observacion;
        this.reprogramacion = reprogramacion;
    }

    public Integer getIdProgramacion() {
        return idProgramacion;
    }

    public void setIdProgramacion(Integer idProgramacion) {
        this.idProgramacion = idProgramacion;
    }

    public int getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(int consecutivo) {
        this.consecutivo = consecutivo;
    }

    public Date getFechaEjecucion() {
        return fechaEjecucion;
    }

    public void setFechaEjecucion(Date fechaEjecucion) {
        this.fechaEjecucion = fechaEjecucion;
    }

    public Date getFechaFinalizado() {
        return fechaFinalizado;
    }

    public void setFechaFinalizado(Date fechaFinalizado) {
        this.fechaFinalizado = fechaFinalizado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getReprogramacion() {
        return reprogramacion;
    }

    public void setReprogramacion(String reprogramacion) {
        this.reprogramacion = reprogramacion;
    }

    public List<Material> getMaterialList() {
        return materialList;
    }

    public void setMaterialList(List<Material> materialList) {
        this.materialList = materialList;
    }

    public Cuenta getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Cuenta idCuenta) {
        this.idCuenta = idCuenta;
    }

    public ActividadTecnica getIdActtecnica() {
        return idActtecnica;
    }

    public void setIdActtecnica(ActividadTecnica idActtecnica) {
        this.idActtecnica = idActtecnica;
    }

    public Persona getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Persona idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProgramacion != null ? idProgramacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Programacion)) {
            return false;
        }
        Programacion other = (Programacion) object;
        if ((this.idProgramacion == null && other.idProgramacion != null) || (this.idProgramacion != null && !this.idProgramacion.equals(other.idProgramacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servitv.entity.Programacion[ idProgramacion=" + idProgramacion + " ]";
    }
    
}
