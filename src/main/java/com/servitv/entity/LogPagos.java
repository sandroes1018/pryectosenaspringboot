/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jlagosmu
 */
@Entity
@Table(name = "log_pagos")
@NamedQueries({
    @NamedQuery(name = "LogPagos.findAll", query = "SELECT l FROM LogPagos l")
    , @NamedQuery(name = "LogPagos.findByIdLogPagos", query = "SELECT l FROM LogPagos l WHERE l.idLogPagos = :idLogPagos")
    , @NamedQuery(name = "LogPagos.findByUsuarioAccion", query = "SELECT l FROM LogPagos l WHERE l.usuarioAccion = :usuarioAccion")
    , @NamedQuery(name = "LogPagos.findByIdFactura", query = "SELECT l FROM LogPagos l WHERE l.idFactura = :idFactura")})
public class LogPagos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idLogPagos")
    private Integer idLogPagos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "usuarioAccion")
    private String usuarioAccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "id_factura")
    private String idFactura;
    @JoinColumn(name = "id_pago", referencedColumnName = "id_pago")
    @ManyToOne(optional = false)
    private Pago idPago;

    public LogPagos() {
    }

    public LogPagos(Integer idLogPagos) {
        this.idLogPagos = idLogPagos;
    }

    public LogPagos(Integer idLogPagos, String usuarioAccion, String idFactura) {
        this.idLogPagos = idLogPagos;
        this.usuarioAccion = usuarioAccion;
        this.idFactura = idFactura;
    }

    public Integer getIdLogPagos() {
        return idLogPagos;
    }

    public void setIdLogPagos(Integer idLogPagos) {
        this.idLogPagos = idLogPagos;
    }

    public String getUsuarioAccion() {
        return usuarioAccion;
    }

    public void setUsuarioAccion(String usuarioAccion) {
        this.usuarioAccion = usuarioAccion;
    }

    public String getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

    public Pago getIdPago() {
        return idPago;
    }

    public void setIdPago(Pago idPago) {
        this.idPago = idPago;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogPagos != null ? idLogPagos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogPagos)) {
            return false;
        }
        LogPagos other = (LogPagos) object;
        if ((this.idLogPagos == null && other.idLogPagos != null) || (this.idLogPagos != null && !this.idLogPagos.equals(other.idLogPagos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servitv.entity.LogPagos[ idLogPagos=" + idLogPagos + " ]";
    }
    
}
