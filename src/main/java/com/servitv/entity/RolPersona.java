/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author jlagosmu
 */
@Entity
@Table(name = "rol_persona")
@NamedQueries({
    @NamedQuery(name = "RolPersona.findAll", query = "SELECT r FROM RolPersona r")
    , @NamedQuery(name = "RolPersona.findByIdPersonaRol", query = "SELECT r FROM RolPersona r WHERE r.idPersonaRol = :idPersonaRol")})
public class RolPersona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_persona_rol")
    private Integer idPersonaRol;
    @JoinColumn(name = "id_Rol", referencedColumnName = "id_Rol")
    @ManyToOne(optional = false)
    private Rol idRol;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuario idUsuario;

    public RolPersona() {
    }

    public RolPersona(Integer idPersonaRol) {
        this.idPersonaRol = idPersonaRol;
    }

    public Integer getIdPersonaRol() {
        return idPersonaRol;
    }

    public void setIdPersonaRol(Integer idPersonaRol) {
        this.idPersonaRol = idPersonaRol;
    }

    public Rol getIdRol() {
        return idRol;
    }

    public void setIdRol(Rol idRol) {
        this.idRol = idRol;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPersonaRol != null ? idPersonaRol.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolPersona)) {
            return false;
        }
        RolPersona other = (RolPersona) object;
        if ((this.idPersonaRol == null && other.idPersonaRol != null) || (this.idPersonaRol != null && !this.idPersonaRol.equals(other.idPersonaRol))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servitv.entity.RolPersona[ idPersonaRol=" + idPersonaRol + " ]";
    }
    
}
