/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jlagosmu
 */
@Entity
@Table(name = "actividad_tecnica")
@NamedQueries({
    @NamedQuery(name = "ActividadTecnica.findAll", query = "SELECT a FROM ActividadTecnica a")
    , @NamedQuery(name = "ActividadTecnica.findByIdActtec", query = "SELECT a FROM ActividadTecnica a WHERE a.idActtec = :idActtec")
    , @NamedQuery(name = "ActividadTecnica.findByTipo", query = "SELECT a FROM ActividadTecnica a WHERE a.tipo = :tipo")
    , @NamedQuery(name = "ActividadTecnica.findByDescripcion", query = "SELECT a FROM ActividadTecnica a WHERE a.descripcion = :descripcion")
    , @NamedQuery(name = "ActividadTecnica.findByEstadoAct", query = "SELECT a FROM ActividadTecnica a WHERE a.estadoAct = :estadoAct")})
public class ActividadTecnica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Act_tec")
    private Integer idActtec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "tipo")
    private String tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "estado_Act")
    private String estadoAct;

    public ActividadTecnica() {
    }

    public ActividadTecnica(Integer idActtec) {
        this.idActtec = idActtec;
    }

    public ActividadTecnica(Integer idActtec, String tipo, String descripcion, String estadoAct) {
        this.idActtec = idActtec;
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.estadoAct = estadoAct;
    }

    public Integer getIdActtec() {
        return idActtec;
    }

    public void setIdActtec(Integer idActtec) {
        this.idActtec = idActtec;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstadoAct() {
        return estadoAct;
    }

    public void setEstadoAct(String estadoAct) {
        this.estadoAct = estadoAct;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idActtec != null ? idActtec.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActividadTecnica)) {
            return false;
        }
        ActividadTecnica other = (ActividadTecnica) object;
        if ((this.idActtec == null && other.idActtec != null) || (this.idActtec != null && !this.idActtec.equals(other.idActtec))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servitv.entity.ActividadTecnica[ idActtec=" + idActtec + " ]";
    }
    
}
