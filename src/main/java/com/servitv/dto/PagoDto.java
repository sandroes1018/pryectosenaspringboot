/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import com.servitv.entity.Pago;
import java.util.Date;

/**
 *
 * @author jlagosmu
 */
public class PagoDto {

    private Integer idPago;
    private Date fechaPago;
    private double valor;
    private String abono;
    private String descripcion;
    private FacturaDto factura; 

    /* datos pago */
    private double saldoPago;
    private int idFactura;

    
    public PagoDto() {
       
    }

    public PagoDto(Pago pago) {
        this.idPago = pago.getIdPago();
        this.fechaPago = pago.getFechaPago();
        this.valor = pago.getValor();
        this.abono = pago.getAbono();
        this.descripcion = pago.getDescripcion();
       
    }

    public FacturaDto getFactura() {
        return factura;
    }

    public void setFactura(FacturaDto factura) {
        this.factura = factura;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public double getSaldoPago() {
        return saldoPago;
    }

    public void setSaldoPago(double saldoPago) {
        this.saldoPago = saldoPago;
    }
    
    

    public Integer getIdPago() {
        return idPago;
    }

    public void setIdPago(Integer idPago) {
        this.idPago = idPago;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getAbono() {
        return abono;
    }

    public void setAbono(String abono) {
        this.abono = abono;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

   
}
