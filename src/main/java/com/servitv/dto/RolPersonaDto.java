/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import com.servitv.entity.RolPersona;
import java.io.Serializable;

/**
 *
 * @author jlagosmu
 */
public class RolPersonaDto implements Serializable {

    private Integer idPersonaRol;
    private RolDto rolDto;
    private UsuarioDto usuarioDto;

    public RolPersonaDto() {
        this.rolDto = new RolDto();
    }

    public RolPersonaDto(RolPersona rolPersona) {
        this.idPersonaRol = rolPersona.getIdPersonaRol();
        this.rolDto = new RolDto();
        this.usuarioDto = new UsuarioDto();
    }

    public Integer getIdPersonaRol() {
        return idPersonaRol;
    }

    public void setIdPersonaRol(Integer idPersonaRol) {
        this.idPersonaRol = idPersonaRol;
    }

    public RolDto getRolDto() {
        return rolDto;
    }

    public void setRolDto(RolDto rolDto) {
        this.rolDto = rolDto;
    }

    public UsuarioDto getUsuarioDto() {
        return usuarioDto;
    }

    public void setUsuarioDto(UsuarioDto usuarioDto) {
        this.usuarioDto = usuarioDto;
    }

}
