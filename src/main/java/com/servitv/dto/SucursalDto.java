/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import com.servitv.entity.Sucursal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jlagosmu
 */

public class SucursalDto{

    private Integer idSucursal;
    private String nombre;
    private String direccion;
    private String telefono;
    private CuentaDto cuentaDto;
    private List<PersonaDto> personaList;

    public SucursalDto() {
       this.cuentaDto = new CuentaDto();
        this.personaList = new ArrayList();
    }

    
    
    
    public SucursalDto(Sucursal sucursal) {
        this.idSucursal = sucursal.getIdSucursal();
        this.nombre = sucursal.getNombre();
        this.direccion = sucursal.getDireccion();
        this.telefono = sucursal.getTelefono();
        this.personaList = new ArrayList();
    }

      
    
    public Integer getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(Integer idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public List<PersonaDto> getPersonaList() {
        return personaList;
    }

    public void setPersonaList(List<PersonaDto> personaList) {
        this.personaList = personaList;
    }
   
}
