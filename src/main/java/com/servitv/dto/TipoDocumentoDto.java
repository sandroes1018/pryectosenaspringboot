/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import com.servitv.entity.TipoDocumento;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jlagosmu
 */

public class TipoDocumentoDto  {

    private Integer idTipo;
    private String tipo;
    private String siglas;
    private List<PersonaDto> personaList;

    public TipoDocumentoDto() {
        this.personaList = new ArrayList();
    }

    public TipoDocumentoDto(TipoDocumento tipoDocumento) {
        this.idTipo = tipoDocumento.getIdTipo();
        this.tipo = tipoDocumento.getTipo();
        this.siglas = tipoDocumento.getSiglas();
        this.personaList = new ArrayList();
    }
    
    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

    public List<PersonaDto> getPersonaList() {
        return personaList;
    }

    public void setPersonaList(List<PersonaDto> personaList) {
        this.personaList = personaList;
    }

   
}
