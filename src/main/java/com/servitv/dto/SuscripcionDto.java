/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import com.servitv.entity.Suscripcion;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jlagosmu
 */
public class SuscripcionDto {

    private Integer idSuscripcion;
    private double valorTotal;
    private Date fecha;
    private PersonaDto idPersonaDto;
    private CuentaDto cuentaDto;
    private List<ServicioDto> servicioList;

    public SuscripcionDto() {
        this.cuentaDto = new CuentaDto();
    }

   
    

   

    public SuscripcionDto(Suscripcion suscripcion) {
        this.idSuscripcion = suscripcion.getIdSuscripcion();
        this.valorTotal = suscripcion.getValorTotal();
        this.fecha = suscripcion.getFecha();
        this.servicioList = new ArrayList();
        this.idPersonaDto = new PersonaDto();
        this.cuentaDto = new CuentaDto();
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<ServicioDto> getServicioList() {
        return servicioList;
    }

    public void setServicioList(List<ServicioDto> servicioList) {
        this.servicioList = servicioList;
    }

    public Integer getIdSuscripcion() {
        return idSuscripcion;
    }

    public void setIdSuscripcion(Integer idSuscripcion) {
        this.idSuscripcion = idSuscripcion;
    }

    public PersonaDto getIdPersonaDto() {
        return idPersonaDto;
    }

    public void setIdPersonaDto(PersonaDto idPersonaDto) {
        this.idPersonaDto = idPersonaDto;
    }

    public CuentaDto getCuentaDto() {
        return cuentaDto;
    }

    public void setCuentaDto(CuentaDto cuentaDto) {
        this.cuentaDto = cuentaDto;
    }

}
