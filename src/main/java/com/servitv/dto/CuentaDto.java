/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import com.servitv.entity.Cuenta;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jlagosmu
 */
public class CuentaDto {

    private Integer idCuenta;
    private String estadoCuen;
    private List<FacturaDto> facturaList;
    private List<EquipoDto> equipoList;
    private SuscripcionDto idSuscripcion;
    private List<ProgramacionDto> programacionList;
    private String idenCliente;

    public CuentaDto() {
        this.facturaList = new ArrayList();
        this.equipoList = new ArrayList();
        this.programacionList = new ArrayList();    
    }

    public CuentaDto(Cuenta cuenta) {
        this.idCuenta = cuenta.getIdCuenta();
        this.estadoCuen = cuenta.getEstadoCuen();
        this.facturaList = new ArrayList();
        this.equipoList = new ArrayList();
        this.idSuscripcion = new SuscripcionDto();
        this.programacionList = new ArrayList();
    }
        

    public String getIdenCliente() {
		return idenCliente;
	}

	public void setIdenCliente(String idenCliente) {
		this.idenCliente = idenCliente;
	}

	public List<FacturaDto> getFacturaList() {
        return facturaList;
    }

    public void setFacturaList(List<FacturaDto> facturaList) {
        this.facturaList = facturaList;
    }

    public SuscripcionDto getIdSuscripcion() {
        return idSuscripcion;
    }

    public void setIdSuscripcion(SuscripcionDto idSuscripcion) {
        this.idSuscripcion = idSuscripcion;
    }

    public Integer getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Integer idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getEstadoCuen() {
        return estadoCuen;
    }

    public void setEstadoCuen(String estadoCuen) {
        this.estadoCuen = estadoCuen;
    }

    public List<EquipoDto> getEquipoList() {
        return equipoList;
    }

    public void setEquipoList(List<EquipoDto> equipoList) {
        this.equipoList = equipoList;
    }

    public List<ProgramacionDto> getProgramacionList() {
        return programacionList;
    }

    public void setProgramacionList(List<ProgramacionDto> programacionList) {
        this.programacionList = programacionList;
    }

}
