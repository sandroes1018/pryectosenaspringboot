/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import com.servitv.entity.Rol;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jlagosmu
 */
public class RolDto {

    private Integer idRol;
    private String nombre;
    private String descripcion;
    private List<PermisoDto> permisoList;
    private List<RolPersonaDto> personaList;

    public RolDto() {
        this.permisoList = new ArrayList();
        this.personaList = new ArrayList();
    }

    public RolDto(Rol rol) {
        this.idRol = rol.getIdRol();
        this.nombre = rol.getNombre();
        this.descripcion = rol.getDescripcion();
        this.permisoList = new ArrayList();
        this.personaList = new ArrayList();
    }
    
    

    
    
    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<PermisoDto> getPermisoList() {
        return permisoList;
    }

    public void setPermisoList(List<PermisoDto> permisoList) {
        this.permisoList = permisoList;
    }

    public List<RolPersonaDto> getPersonaList() {
        return personaList;
    }

    public void setPersonaList(List<RolPersonaDto> personaList) {
        this.personaList = personaList;
    }

  
   

}
