/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import java.util.List;

/**
 *
 * @author jlagosmu
 */

public class ActividadtecnicaDto {

    private Integer idActtec;
    private String tipo;
    private String descripcion;
    private String estadoAct;
    private String actividadTecnicacol;
    private PersonaDto persona;
    private List<ProgramacionDto> programacion;

    public Integer getIdActtec() {
        return idActtec;
    }

    public void setIdActtec(Integer idActtec) {
        this.idActtec = idActtec;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstadoAct() {
        return estadoAct;
    }

    public void setEstadoAct(String estadoAct) {
        this.estadoAct = estadoAct;
    }

    public String getActividadTecnicacol() {
        return actividadTecnicacol;
    }

    public void setActividadTecnicacol(String actividadTecnicacol) {
        this.actividadTecnicacol = actividadTecnicacol;
    }

    public PersonaDto getPersona() {
        return persona;
    }

    public void setPersona(PersonaDto persona) {
        this.persona = persona;
    }

	public List<ProgramacionDto> getProgramacion() {
		return programacion;
	}

	public void setProgramacion(List<ProgramacionDto> programacion) {
		this.programacion = programacion;
	}
    
    
    
}
