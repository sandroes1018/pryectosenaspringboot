/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import com.servitv.entity.Factura;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jlagosmu
 */
public class FacturaDto {

    private Integer idFactura;
    private String descripcion;
    private Date fecha;
    private double valor;
    private String estado;
    private double saldo;
    private List<PagoDto> pagoList;
    private CuentaDto cuentaDto;

    public FacturaDto() {
        this.pagoList = new ArrayList();
        this.cuentaDto = new CuentaDto();
    }

    public FacturaDto(Factura factura) {
        this.idFactura = factura.getIdFactura();
        this.descripcion = factura.getDescripcion();
        this.fecha = factura.getFecha();
        this.valor = factura.getValor();
        this.estado = factura.getEstado();
        this.saldo = factura.getSaldo();
        this.pagoList = new ArrayList();
        this.cuentaDto = new CuentaDto();
        this.saldo = factura.getSaldo();
    }

    
    
    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

   
    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<PagoDto> getPagoList() {
        return pagoList;
    }

    public void setPagoList(List<PagoDto> pagoList) {
        this.pagoList = pagoList;
    }

    public CuentaDto getCuentaDto() {
        return cuentaDto;
    }

    public void setCuentaDto(CuentaDto cuentaDto) {
        this.cuentaDto = cuentaDto;
    }

    

}
