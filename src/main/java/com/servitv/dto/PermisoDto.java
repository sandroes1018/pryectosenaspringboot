/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;


import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jlagosmu
 */

public class PermisoDto {

    private Integer idPermisos;
    private String nombre;
    private String url;
    private String incon;
    private List<RolDto> rolList;
    private List<PermisoDto> permisoList;
    private PermisoDto idPermisopadre;

    public PermisoDto() {
        this.rolList = new ArrayList();
        this.permisoList = new ArrayList();
        this.idPermisopadre = new PermisoDto();
    }
    
    public Integer getIdPermisos() {
        return idPermisos;
    }

    public void setIdPermisos(Integer idPermisos) {
        this.idPermisos = idPermisos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIncon() {
        return incon;
    }

    public void setIncon(String incon) {
        this.incon = incon;
    }

    public List<RolDto> getRolList() {
        return rolList;
    }

    public void setRolList(List<RolDto> rolList) {
        this.rolList = rolList;
    }

    public List<PermisoDto> getPermisoList() {
        return permisoList;
    }

    public void setPermisoList(List<PermisoDto> permisoList) {
        this.permisoList = permisoList;
    }

    public PermisoDto getIdPermisopadre() {
        return idPermisopadre;
    }

    public void setIdPermisopadre(PermisoDto idPermisopadre) {
        this.idPermisopadre = idPermisopadre;
    }

   
}
