/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

/**
 *
 * @author jlagosmu
 */

public class MaterialDto {

    private Integer idMaterial;
    private int codigo;
    private String descripcion;
    private int cantidad;
    private ProgramacionDto programacion;

    public MaterialDto() {
        this.programacion = new ProgramacionDto();
    }

    
    public Integer getIdMaterial() {
        return idMaterial;
    }

    public void setIdMaterial(Integer idMaterial) {
        this.idMaterial = idMaterial;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public ProgramacionDto getProgramacion() {
        return programacion;
    }

    public void setProgramacion(ProgramacionDto programacion) {
        this.programacion = programacion;
    }

    
    
}
