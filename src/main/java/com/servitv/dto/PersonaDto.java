/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import com.servitv.entity.Persona;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jlagosmu
 */
public class PersonaDto {

    private Integer idPersona;
    private String numeroDocumento;
    private String nombre;
    private String apellido;
    private String direccion;
    private String telefono;
    private String cargo;
    private SucursalDto sucursalDto;
    private TipoDocumentoDto tipoDocumentoDto;
    private SuscripcionDto suscripcionDto;
    private List<ProgramacionDto> programacionList;
    private UsuarioDto usuarioDto;
    private String email;

    /* datos suscripcion*/
    private String[] serviSuscripcion;
    private int idTipo;
    private int idSucursal;
    private CuentaDto cuentaDto;
    private int valorTotal;

    public PersonaDto() {

    }

    public PersonaDto(Persona persona) {
        this.idPersona = persona.getIdPersona();
        this.numeroDocumento = persona.getNumeroDocumento();
        this.nombre = persona.getNombre();
        this.apellido = persona.getApellido();
        this.direccion = persona.getDireccion();
        this.telefono = persona.getTelefono();
        this.cargo = persona.getCargo();
        this.email = persona.getEmail();
        this.sucursalDto = new SucursalDto();
        this.tipoDocumentoDto = new TipoDocumentoDto();
        this.usuarioDto = new UsuarioDto();
        this.suscripcionDto = new SuscripcionDto();
        this.programacionList = new ArrayList();

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    public TipoDocumentoDto getTipoDocumentoDto() {
        return tipoDocumentoDto;
    }

    public int getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(int valorTotal) {
        this.valorTotal = valorTotal;
    }

    public void setTipoDocumentoDto(TipoDocumentoDto tipoDocumentoDto) {
        this.tipoDocumentoDto = tipoDocumentoDto;
    }

    public SuscripcionDto getSuscripcionDto() {
        return suscripcionDto;
    }

    public void setSuscripcionDto(SuscripcionDto suscripcionDto) {
        this.suscripcionDto = suscripcionDto;
    }

    public UsuarioDto getUsuarioDto() {
        return usuarioDto;
    }

    public void setUsuarioDto(UsuarioDto usuarioDto) {
        this.usuarioDto = usuarioDto;
    }

    public SucursalDto getSucursalDto() {
        return sucursalDto;
    }

    public void setSucursalDto(SucursalDto sucursalDto) {
        this.sucursalDto = sucursalDto;
    }

    public List<ProgramacionDto> getProgramacionList() {
        return programacionList;
    }

    public void setProgramacionList(List<ProgramacionDto> programacionList) {
        this.programacionList = programacionList;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String[] getServiSuscripcion() {
        return serviSuscripcion;
    }

    public void setServiSuscripcion(String[] serviSuscripcion) {
        this.serviSuscripcion = serviSuscripcion;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public int getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(int idSucursal) {
        this.idSucursal = idSucursal;
    }

    public CuentaDto getCuentaDto() {
        return cuentaDto;
    }

    public void setCuentaDto(CuentaDto cuentaDto) {
        this.cuentaDto = cuentaDto;
    }

}
