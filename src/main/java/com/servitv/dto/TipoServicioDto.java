/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import com.servitv.entity.TipoServicio;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jlagosmu
 */
public class TipoServicioDto {
    
    private Integer idTipoServicio;
    private String nombre;
    private String descripcion;
    private double valor;
    private List<ServicioDto> servicioList;

    public TipoServicioDto() {
        this.servicioList = new ArrayList();
    }

    public TipoServicioDto( TipoServicio tipoServicio) {
        this.idTipoServicio = tipoServicio.getIdTipoServicio();
        this.nombre = tipoServicio.getNombre();
        this.descripcion = tipoServicio.getDescripcion();
        this.valor = tipoServicio.getValor();
        this.servicioList = new ArrayList();
    }

    public Integer getIdTipoServicio() {
        return idTipoServicio;
    }

    public void setIdTipoServicio(Integer idTipoServicio) {
        this.idTipoServicio = idTipoServicio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<ServicioDto> getServicioList() {
        return servicioList;
    }

    public void setServicioList(List<ServicioDto> servicioList) {
        this.servicioList = servicioList;
    }

    
    
}
