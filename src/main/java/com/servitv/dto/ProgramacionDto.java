/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.servitv.entity.ActividadTecnica;
import com.servitv.entity.Cuenta;
import com.servitv.entity.Material;
import com.servitv.entity.Persona;
import com.servitv.entity.Programacion;

/**
 *
 * @author jlagosmu
 */
public class ProgramacionDto implements Serializable {

	private Integer idProgramacion;
	private int consecutivo;
	private Date fechaEjecucion;
	private Date fechaFinalizado;
	private String observacion;
	private String reprogramacion;
	private List<MaterialDto> materialList;
	private CuentaDto cuenta;
	private ActividadtecnicaDto actividadtecnica;
	private PersonaDto persona;

	public ProgramacionDto() {
		this.materialList = new ArrayList();
		this.cuenta = new CuentaDto();
		this.actividadtecnica = new ActividadtecnicaDto();
		this.persona = new PersonaDto();
	}

	public ProgramacionDto(ActividadTecnica ac, Cuenta cu, Persona per, List<Material> ma,Programacion pro) {
		
		this.materialList = new ArrayList();
		this.cuenta = new CuentaDto();
		this.actividadtecnica = new ActividadtecnicaDto();
		this.persona = new PersonaDto();
		
		this.actividadtecnica.setIdActtec(ac.getIdActtec());
		this.actividadtecnica.setTipo(ac.getTipo());
		this.actividadtecnica.setDescripcion(ac.getDescripcion());
		this.actividadtecnica.setEstadoAct(ac.getEstadoAct());

		this.cuenta.setIdCuenta(cu.getIdCuenta());
		this.cuenta.setEstadoCuen(cu.getEstadoCuen());

		this.persona.setIdPersona(per.getIdPersona());
		this.persona.setNumeroDocumento(per.getNumeroDocumento());
		this.persona.setNombre(per.getNombre());
		this.persona.setApellido(per.getApellido());
		this.persona.setDireccion(per.getDireccion());
		this.persona.setTelefono(per.getTelefono());
		this.persona.setCargo(per.getCargo());

		for (Material material : ma) {
			MaterialDto item = new MaterialDto();
				item.setIdMaterial(material.getIdMaterial());
				item.setDescripcion(material.getDescripcion());
				item.setCantidad(material.getCantidad());
		}
		
		this.idProgramacion = pro.getIdProgramacion();
		this.consecutivo = pro.getConsecutivo();
		this.fechaEjecucion = pro.getFechaEjecucion();
		this.fechaFinalizado = pro.getFechaFinalizado();
		this.observacion = pro.getObservacion();
		this.reprogramacion = pro.getReprogramacion();
		
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public int getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(int consecutivo) {
		this.consecutivo = consecutivo;
	}

	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public Date getFechaFinalizado() {
		return fechaFinalizado;
	}

	public void setFechaFinalizado(Date fechaFinalizado) {
		this.fechaFinalizado = fechaFinalizado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getReprogramacion() {
		return reprogramacion;
	}

	public void setReprogramacion(String reprogramacion) {
		this.reprogramacion = reprogramacion;
	}

	public List<MaterialDto> getMaterialList() {
		return materialList;
	}

	public void setMaterialList(List<MaterialDto> materialList) {
		this.materialList = materialList;
	}

	public CuentaDto getCuenta() {
		return cuenta;
	}

	public void setCuenta(CuentaDto cuenta) {
		this.cuenta = cuenta;
	}

	public ActividadtecnicaDto getActividadtecnica() {
		return actividadtecnica;
	}

	public void setActividadtecnica(ActividadtecnicaDto actividadtecnica) {
		this.actividadtecnica = actividadtecnica;
	}

	public PersonaDto getPersona() {
		return persona;
	}

	public void setPersona(PersonaDto persona) {
		this.persona = persona;
	}

}
