/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.dto;

/**
 *
 * @author jlagosmu
 */
public class ServicioDto {

    private Integer idServicio;
    private String estadoServ;
    private SuscripcionDto supscripcion;
    private TipoServicioDto tipoServicio;

    public ServicioDto() {
        this.supscripcion = new SuscripcionDto();
        this.tipoServicio = new TipoServicioDto();
    }


    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getEstadoServ() {
        return estadoServ;
    }

    public void setEstadoServ(String estadoServ) {
        this.estadoServ = estadoServ;
    }

    public SuscripcionDto getSupscripcion() {
        return supscripcion;
    }

    public void setSupscripcion(SuscripcionDto supscripcion) {
        this.supscripcion = supscripcion;
    }

    public TipoServicioDto getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(TipoServicioDto tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

}
