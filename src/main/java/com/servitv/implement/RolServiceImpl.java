/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.RolDto;
import com.servitv.entity.Rol;
import com.servitv.newpackage.repository.dao.RolRepositoryDao;
import com.servitv.service.interfaz.RolService;
import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public class RolServiceImpl implements RolService {

    private RolDto rolDto;
    @Autowired private RolRepositoryDao rolRepositoryDao;

    /**
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<RolDto> listaRoles() throws Exception {
        List<RolDto> roles = new ArrayList();
        try {
            for (Rol rol : rolRepositoryDao.findAll()) {
                rolDto = new RolDto(rol);
                rolDto.setPermisoList(null);
                rolDto.setPersonaList(null);
                roles.add(rolDto);
            }
        } catch (Exception e) {
            Logger.getLogger(PersonaSeviceIpml.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
        return roles;
    }

}
