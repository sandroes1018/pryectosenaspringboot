/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.FacturaDto;
import com.servitv.dto.PagoDto;
import com.servitv.entity.Factura;
import com.servitv.entity.Pago;
import com.servitv.newpackage.repository.dao.FacturaRepositoryDao;
import com.servitv.newpackage.repository.dao.PagoRepositoryDao;
import com.servitv.service.interfaz.PagoSevice;
import com.servitv.util.ConstantesServitv;
import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public class PagoSeviceImpl implements PagoSevice {

    private Pago pago;
    private PagoDto pagoDto;
    @Autowired
    private PagoRepositoryDao pagoRepositoryDao;
    @Autowired
    private FacturaRepositoryDao facturaRepositoryDao;

    @Override
    public boolean registroPago(PagoDto pagoDto) throws Exception {
        Factura factura = new Factura();
        try {
            factura = facturaRepositoryDao.findById(pagoDto.getIdFactura()).get();
            pago = convertirDtoEntity(pagoDto);
            if (pagoDto.getSaldoPago() == 0) {
                factura.setEstado(ConstantesServitv.ESTADO_PAGADA);
                pago.setAbono("no");
            } else if (pagoDto.getSaldoPago() < 0) {
                factura.setEstado(ConstantesServitv.ESTADO_SOBRE_PAGDADA);
                pago.setAbono("no");
            } else {
                factura.setEstado(ConstantesServitv.ESTADO_ABONADA);
                pago.setAbono("si");
            }
            factura.setSaldo(pagoDto.getSaldoPago());
            pago.setIdFactura(factura);
            pagoRepositoryDao.save(pago);
            facturaRepositoryDao.save(factura);
            return true;
        } catch (Exception e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
    }

    @Override
    public List<PagoDto> listaPagos() throws Exception {

        try {
            List<PagoDto> listaPagos = new ArrayList();
            for (Pago pag : pagoRepositoryDao.findAll()) {
                pagoDto = new PagoDto(pag);
                pagoDto.setFactura(new FacturaDto(pag.getIdFactura()));
                listaPagos.add(pagoDto);
            }
            return listaPagos;
        } catch (Exception e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }

    }

    private Pago convertirDtoEntity(PagoDto pagoDto) {
        pago = new Pago();
        pago.setIdPago(pagoDto.getIdPago());
        pago.setFechaPago(pagoDto.getFechaPago());
        pago.setValor(pagoDto.getValor());
        pago.setAbono(pagoDto.getAbono());
        pago.setDescripcion(pagoDto.getDescripcion());

        return pago;
    }
}
