package com.servitv.implement;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.servitv.dto.ActividadtecnicaDto;
import com.servitv.dto.CuentaDto;
import com.servitv.dto.MaterialDto;
import com.servitv.dto.ProgramacionDto;
import com.servitv.dto.TipoServicioDto;
import com.servitv.entity.ActividadTecnica;
import com.servitv.entity.Cuenta;
import com.servitv.entity.Material;
import com.servitv.entity.Persona;
import com.servitv.entity.Programacion;
import com.servitv.newpackage.repository.dao.ActividadTecnicaRepositiryDao;
import com.servitv.newpackage.repository.dao.ControlMaterialRepositoryDao;
import com.servitv.newpackage.repository.dao.ControlProgramacionRepositoryDao;
import com.servitv.newpackage.repository.dao.CuentaRepositoryDao;
import com.servitv.newpackage.repository.dao.PersonaRepositoryDao;
import com.servitv.service.interfaz.ProgramacionService;

@Service
public class ProgramacionImpl implements ProgramacionService {

	private Programacion programacion;

	private Material material;

	private ActividadTecnica actividadTecnica;

	@Autowired
	private ControlProgramacionRepositoryDao controlProgramacionRepositoryDao;
	@Autowired
	private CuentaRepositoryDao cuentaRepositoryDao;
	@Autowired
	private ControlMaterialRepositoryDao controlMaterialRepositoryDao;
	@Autowired
	private ActividadTecnicaRepositiryDao actividadTecnicaRepositiryDao;
	@Autowired
	private PersonaRepositoryDao personaRepositoryDao;

	@Override
	@Transactional
	public boolean newProgramacion(MaterialDto pr) throws Exception {
		// TODO Auto-generated method stub
		try {
			convertirDtoEntidad(pr);

			actividadTecnicaRepositiryDao.save(actividadTecnica);

			programacion.setIdCuenta(cuentaRepositoryDao.getOne(pr.getProgramacion().getCuenta().getIdCuenta()));
			programacion.setIdActtecnica(actividadTecnica);
			programacion.setIdPersona(personaRepositoryDao.getOne(pr.getProgramacion().getPersona().getIdPersona()));

			controlProgramacionRepositoryDao.save(programacion);

			material.setProgramacionIdProgramacion(programacion);

			controlMaterialRepositoryDao.save(material);

			return true;
		} catch (Exception e) {
			System.out.println("Error" + e);
			throw new Exception(e.getMessage());
		}

	}

	private void convertirDtoEntidad(MaterialDto item) {
		
	/*	Gson gson = new Gson();
		
		System.out.println("// 1");
		actividadTecnica = new ActividadTecnica();
		programacion = new Programacion();
		material = new Material();
			
		String JSON = gson.toJson(item.getProgramacion().getActividadtecnica());
		actividadTecnica = gson.fromJson(JSON, ActividadTecnica.class);
		System.out.println("// 2");
		JSON = gson.toJson(item.getProgramacion());
		programacion = gson.fromJson(JSON, Programacion.class);
		System.out.println("// 3");
		JSON = gson.toJson(item);
		material = gson.fromJson(JSON, Material.class);
		System.out.println("// 4"); */
		
		actividadTecnica = new ActividadTecnica();
		programacion = new Programacion();
		material = new Material();

		actividadTecnica.setDescripcion(item.getProgramacion().getActividadtecnica().getDescripcion());
		actividadTecnica.setEstadoAct(item.getProgramacion().getActividadtecnica().getEstadoAct());
		actividadTecnica.setTipo(item.getProgramacion().getActividadtecnica().getEstadoAct());

		programacion.setConsecutivo(item.getProgramacion().getConsecutivo());
		programacion.setFechaEjecucion(item.getProgramacion().getFechaEjecucion());
		programacion.setFechaFinalizado(item.getProgramacion().getFechaFinalizado());
		programacion.setObservacion(item.getProgramacion().getObservacion());
		programacion.setReprogramacion(item.getProgramacion().getReprogramacion());

		material.setCantidad(item.getCantidad());
		material.setDescripcion(item.getDescripcion());

	}

	@Override
	public List<ProgramacionDto> getProgramacion() throws Exception {
		// TODO Auto-generated method stub
		System.out.println(controlProgramacionRepositoryDao.findAll().size());
		List<ProgramacionDto> list = new ArrayList<>();
		//Gson gson = new Gson();
		for (Programacion programacion : controlProgramacionRepositoryDao.findAll()) {
			
			/*ProgramacionDto item = new ProgramacionDto();
			
			String JSON = gson.toJson(programacion);

			 item = gson.fromJson(JSON, ProgramacionDto.class); */
			
			ProgramacionDto item = new ProgramacionDto(programacion.getIdActtecnica(),programacion.getIdCuenta(),programacion.getIdPersona(),programacion.getMaterialList(),programacion);	
		
			list.add(item);
		}
		return list;
	}

	@Override
	public ProgramacionDto getProgramacionId(int id) throws Exception {
		// TODO Auto-generated method stub
		Programacion programacion = controlProgramacionRepositoryDao.getOne(id);

		ProgramacionDto item = new ProgramacionDto();
		item.setIdProgramacion(programacion.getIdProgramacion());
		item.setConsecutivo(programacion.getConsecutivo());
		item.setFechaEjecucion(programacion.getFechaEjecucion());
		item.setFechaFinalizado(programacion.getFechaFinalizado());
		item.setObservacion(programacion.getObservacion());
		item.setReprogramacion(programacion.getReprogramacion());
		CuentaDto cuentaDto = new CuentaDto();
		cuentaDto.setIdCuenta(programacion.getIdCuenta().getIdCuenta());
		item.setCuenta(cuentaDto);

		return item;
	}

}
