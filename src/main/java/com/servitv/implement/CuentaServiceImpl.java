/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.CuentaDto;
import com.servitv.dto.PersonaDto;
import com.servitv.entity.Cuenta;
import com.servitv.entity.Persona;
import com.servitv.newpackage.repository.dao.CuentaRepositoryDao;
import com.servitv.newpackage.repository.dao.PersonaRepositoryDao;
import com.servitv.newpackage.repository.dao.SuscripcionRepositoryDao;
import com.servitv.service.interfaz.CuentaService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public class CuentaServiceImpl implements CuentaService {

	private Cuenta cuenta;
    @Autowired
    private CuentaRepositoryDao cuentaRepositoryDao;
    @Autowired
    private PersonaRepositoryDao personaRepositoryDao;
    @Autowired
    private SuscripcionRepositoryDao suscripcionRepositoryDao;
    
    @Override
    public List<CuentaDto> listaCuentas() {
        List<CuentaDto>  listaCuentas = new ArrayList();
        for (Cuenta cuenta : cuentaRepositoryDao.findAll()) {
            listaCuentas.add(new CuentaDto(cuenta));
        }
        return  listaCuentas;
    }

	@Override
	public boolean cuentaMigracion(List<CuentaDto> list) throws Exception {
			
		for (CuentaDto cuentaDto : list) {
			convertirDtoEntity(cuentaDto);			
			Persona p= personaRepositoryDao.findUsuarioCliente(cuentaDto.getIdenCliente());			
			cuenta.setIdSuscripcion(suscripcionRepositoryDao.findByIdCliente(p.getIdPersona()));			
			cuentaRepositoryDao.save(cuenta);			
		}
		
		return true;
	}
	
	@SuppressWarnings("unused")
	private void convertirDtoEntity(CuentaDto cuentaDto) {
		cuenta = new Cuenta();
		cuenta.setEstadoCuen(cuentaDto.getEstadoCuen());
		
	}

}
