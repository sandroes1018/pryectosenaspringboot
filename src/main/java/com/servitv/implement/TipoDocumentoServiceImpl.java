/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.TipoDocumentoDto;
import com.servitv.entity.TipoDocumento;
import com.servitv.newpackage.repository.dao.TipoDocumentoRepositoryDao;
import com.servitv.service.interfaz.TipoDocumentoService;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public class TipoDocumentoServiceImpl implements TipoDocumentoService {

    private TipoDocumento tipoDocumento;
    private TipoDocumentoDto tipoDocumentoDto;

    @Autowired
    private TipoDocumentoRepositoryDao tipoDocumentoRepositoryDao;

    public TipoDocumentoRepositoryDao getTipoDocumentoRepositoryDao() {
        return tipoDocumentoRepositoryDao;
    }

    public void setTipoDocumentoRepositoryDao(TipoDocumentoRepositoryDao tipoDocumentoRepositoryDao) {
        this.tipoDocumentoRepositoryDao = tipoDocumentoRepositoryDao;
    }

    public TipoDocumentoServiceImpl() {
        this.tipoDocumento = new TipoDocumento();
        this.tipoDocumentoDto = new TipoDocumentoDto();
    }

    @Override
    @Transactional
    public List<TipoDocumentoDto> listaTipoDocumento() throws Exception {
        try {
            List<TipoDocumentoDto> listaTipoDocumentoDto = new ArrayList();
            for (TipoDocumento tipoDocumento : tipoDocumentoRepositoryDao.findAll()) {
                listaTipoDocumentoDto.add(new TipoDocumentoDto(tipoDocumento));
            }
            return listaTipoDocumentoDto;
        } catch (Exception e) {
            Logger.getLogger(TipoDocumentoServiceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
    }

}
