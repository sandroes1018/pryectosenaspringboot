/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.UsuarioDto;
import com.servitv.entity.RolPersona;
import com.servitv.entity.Usuario;
import com.servitv.newpackage.repository.dao.UsuarioRepositoryDao;
import com.servitv.service.interfaz.LoginService;
import com.servitv.util.ConstantesServitv;
import java.util.HashMap;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public class LoginServiceImpl implements LoginService {

    private Usuario usuario;
    private UsuarioDto usuarioDto;

    @Autowired
    private UsuarioRepositoryDao usuarioRepositoryDao;

    /**
     *
     * @param user
     * @return
     * @throws Exception
     */
    @Override
    public HashMap loginUser(UsuarioDto user) throws Exception {
        boolean validacion = false;
        try {
            HashMap<String, String> userLogin = new HashMap();
            usuario = usuarioRepositoryDao.findUsuarioPassword(
                    user.getNombre().toLowerCase(), user.getPassword(),
                    ConstantesServitv.ESTADO_ACTIVO);
            if (usuario != null) {
                validacion = true;
                userLogin.put("usuario", usuario.getNombre());

                if (usuario.getRolPersonaList().size() > 0) {

                    for (RolPersona rolPersona : usuario.getRolPersonaList()) {
                        if (rolPersona.getIdRol().getNombre().equals(ConstantesServitv.CLIENTE)) {
                            userLogin.put("ruta", ConstantesServitv.RUTA_CLIENTE);
                        } else {
                            userLogin.put("ruta", ConstantesServitv.RUTA_ADMIN);
                        }
                    }
                } else {
                    throw new Exception(ConstantesServitv.ERROR_LOGIN);
                }
            }
            if (validacion) {
                return userLogin;
            } else {
                throw new Exception(ConstantesServitv.ERROR_LOGIN);
            }
        } catch (Exception e) {

            if (e.getMessage().contains(ConstantesServitv.ERROR_LOGIN)) {
                Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                        .log(Logger.Level.ERROR, e);
                throw new Exception(ConstantesServitv.ERROR_LOGIN);
            } else {
                Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                        .log(Logger.Level.ERROR, e);
                throw new Exception("Error inesperado");
            }

        }
    }

    /**
     *
     * @param usuarioDto
     * @return
     */
    public Usuario convertirDtoEntity(UsuarioDto usuarioDto) {
        usuario = new Usuario();
        usuario.setPassword(usuarioDto.getPassword());
        usuario.setNombre(usuarioDto.getNombre());

        return usuario;
    }
}
