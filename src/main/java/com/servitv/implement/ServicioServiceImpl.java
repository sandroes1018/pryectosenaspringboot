/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.ServicioDto;
import com.servitv.entity.Servicio;
import com.servitv.entity.Suscripcion;
import com.servitv.entity.TipoServicio;
import com.servitv.newpackage.repository.dao.ServicioRepositoryDao;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public class ServicioServiceImpl {

    private Servicio servicio;
    @Autowired
    private ServicioRepositoryDao servicioRepositoryDao;

    public boolean crearClienteServicio(ServicioDto servicioDto, String[] idServicios)
            throws Exception {
        try {
            for (int i = 0; i < idServicios.length; i++) {
                servicio = convertirDtoEntiry(servicioDto);
                servicio.setIdSupscripcion(new Suscripcion(
                        servicioDto.getIdServicio()));
                servicio.setIdTipoServicio(new TipoServicio(
                        Integer.parseInt(idServicios[i])));
                servicioRepositoryDao.save(servicio);
            }
            return true;
        } catch (NumberFormatException e) {
            Logger.getLogger(ServicioServiceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error tipp dato");
        } catch (Exception e) {
            Logger.getLogger(ServicioServiceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }

    }

    private Servicio convertirDtoEntiry(ServicioDto servicioDto) {
        servicio = new Servicio();
        servicio.setEstadoServ(servicioDto.getEstadoServ());
        return servicio;
    }
}
