/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.TipoServicioDto;
import com.servitv.entity.TipoServicio;
import com.servitv.newpackage.repository.dao.ControlServicioRepositoryDao;
import com.servitv.service.interfaz.ControlServiciosSevice;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ControlServiciosSeviceImpl implements ControlServiciosSevice {

    private TipoServicio tipoServicio;
    private TipoServicioDto tipoServicioDto;
    @Autowired
    private ControlServicioRepositoryDao controlServicioRepositoryDao;

    public ControlServicioRepositoryDao getControlServicioRepositoryDao() {
        return controlServicioRepositoryDao;
    }

    /* setter y getter *********************************************************/
    public void setControlServicioRepositoryDao(
            ControlServicioRepositoryDao controlServicioRepositoryDao) {
        this.controlServicioRepositoryDao = controlServicioRepositoryDao;
    }

    public ControlServiciosSeviceImpl() {
        tipoServicioDto = new TipoServicioDto();
        tipoServicio = new TipoServicio();
    }

    public TipoServicio getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(TipoServicio tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    /* metodos *****************************************************************/
    /**
     * crear Servicio
     *
     * @param tipoServicioDto
     */
    @Override
    @Transactional
    public void crearServicio(TipoServicioDto tipoServicioDto) {
        try {
            convertirDtoEntidad(tipoServicioDto);
            controlServicioRepositoryDao.save(tipoServicio);
        } catch (NullPointerException ex) {
            System.out.println("Error : " + ex.getMessage());
        } catch (Exception e) {
            System.out.println("Error : " + e);
        }
    }

    /**
     * listar tipo servicios
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<TipoServicioDto> listaTipoSerivicios() throws Exception {
        try {
            List<TipoServicioDto> listaTipoServicios = new ArrayList();
            for (TipoServicio tipoServicio1E : controlServicioRepositoryDao.findAll()) {
                listaTipoServicios.add(new TipoServicioDto(tipoServicio1E));
            }
            return listaTipoServicios;
        } catch (Exception e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }

    }

    /**
     * buscar Serivicio
     *
     * @param idTipoServicio
     * @return
     * @throws Exception
     */
    @Override
    public TipoServicioDto buscarSerivicio(int idTipoServicio) throws Exception {
        try {
            tipoServicio = (TipoServicio) controlServicioRepositoryDao.findById(idTipoServicio).get();
            tipoServicioDto = new TipoServicioDto(tipoServicio);
            return tipoServicioDto;
        } catch (Exception e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
    }

    /**
     * eliminar TipoSrvicio
     *
     * @param idTipoServicio
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public boolean eliminarTipoSrvicio(int idTipoServicio) throws Exception {
        try {
            tipoServicio = controlServicioRepositoryDao.findById(idTipoServicio).get();
            controlServicioRepositoryDao.delete(tipoServicio);
            return true;
        } catch (Exception e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
    }

    /**
     *
     * @return @throws Exception
     */
    @Override
    public List<TipoServicioDto> listaTipoSeriviciosInternet() throws Exception {
        try {
            List<TipoServicioDto> listaSeriviciosInternet = new ArrayList();
            for (TipoServicio tipoServicio : controlServicioRepositoryDao.findAll()) {
                if (tipoServicio.getDescripcion().equals("internet")) {
                    listaSeriviciosInternet.add(new TipoServicioDto(tipoServicio));
                }
            }
            return listaSeriviciosInternet;
        } catch (Exception e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
    }

    /**
     *
     * @return @throws Exception
     */
    @Override
    public List<TipoServicioDto> listaTipoSeriviciosTv() throws Exception {
        try {
            List<TipoServicioDto> listaSeriviciosTv = new ArrayList();
            for (TipoServicio tipoServicio : controlServicioRepositoryDao.findAll()) {
                if (tipoServicio.getDescripcion().equals("television")) {
                    listaSeriviciosTv.add(new TipoServicioDto(tipoServicio));
                }
            }
            return listaSeriviciosTv;
        } catch (Exception e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
    }

    private void convertirDtoEntidad(TipoServicioDto tipoServicioDto) {
        tipoServicio.setIdTipoServicio(tipoServicioDto.getIdTipoServicio());
        tipoServicio.setNombre(tipoServicioDto.getNombre());
        tipoServicio.setDescripcion(tipoServicioDto.getDescripcion());
        tipoServicio.setValor(tipoServicioDto.getValor());

    }
}
