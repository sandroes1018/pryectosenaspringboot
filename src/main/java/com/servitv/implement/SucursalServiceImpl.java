/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.SucursalDto;
import com.servitv.entity.Sucursal;
import com.servitv.newpackage.repository.dao.SucursalRepositoryDao;
import com.servitv.service.interfaz.SucursalService;
import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public class SucursalServiceImpl implements SucursalService {

    private Sucursal sucursal;
    @Autowired
    private SucursalRepositoryDao sucursalRepositoryDao;

    /**
     *
     * @return @throws Exception
     */
    @Override
    public List<SucursalDto> listaSucursales() throws Exception {
        try {
            List<SucursalDto> listaSucursalDto = new ArrayList();
            for (Sucursal sucursal : sucursalRepositoryDao.findAll()) {
                listaSucursalDto.add(new SucursalDto(sucursal));
            }
            return listaSucursalDto;
        } catch (Exception e) {
            Logger.getLogger(SucursalServiceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }

    }

    public Sucursal convertirDtoEntity(SucursalDto sucursalDto) {
        sucursal = new Sucursal();
        sucursal.setIdSucursal(sucursalDto.getIdSucursal());
        sucursal.setNombre(sucursalDto.getNombre());
        sucursal.setDireccion(sucursalDto.getDireccion());
        sucursal.setTelefono(sucursalDto.getTelefono());
        return sucursal;
    }
}
