/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.CuentaDto;
import com.servitv.dto.FacturaDto;
import com.servitv.dto.PersonaDto;
import com.servitv.dto.ServicioDto;
import com.servitv.dto.SuscripcionDto;
import com.servitv.entity.Factura;
import com.servitv.entity.Persona;
import com.servitv.entity.Sucursal;
import com.servitv.entity.TipoDocumento;
import com.servitv.service.interfaz.PersonaSevice;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.servitv.newpackage.repository.dao.PersonaRepositoryDao;
import com.servitv.newpackage.repository.dao.SucursalRepositoryDao;
import com.servitv.util.ConstantesServitv;
import java.util.Date;
import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

/**
 *
 * @author jlagosmu
 */
@Service
public class PersonaSeviceIpml implements PersonaSevice {

	private Persona persona;
	private SuscripcionDto suscripcionDto;
	private ServicioDto servicioDto;
	private PersonaDto personaDto;
	@Autowired
	private PersonaRepositoryDao personaRepositoryDao;
	@Autowired
	private SuscripcionServiceImpl suscripcionServiceImpl;
	@Autowired
	private ServicioServiceImpl servicioServiceImpl;

	@Override
	public List<PersonaDto> listaClientes() throws Exception {
		try {
			List<PersonaDto> listaClientes = new ArrayList();
			for (Persona persona : personaRepositoryDao.findAll()) {
				if (persona.getCargo().equals(ConstantesServitv.CARGO_CLIENTE)) {
					listaClientes.add(new PersonaDto(persona));
				}
			}
			return listaClientes;
		} catch (Exception e) {
			Logger.getLogger(PersonaSeviceIpml.class.getName()).log(Logger.Level.ERROR, e);
			throw new Exception("Error inesperado");
		}

	}

	@Override
	public List<PersonaDto> listaEmpleado() throws Exception {
		try {
			List<PersonaDto> listaEmpleados = new ArrayList();
			for (Persona persona : personaRepositoryDao.findEmpleados(ConstantesServitv.CARGO_CLIENTE)) {
				personaDto = new PersonaDto(persona);
				personaDto.setSuscripcionDto(null);
				personaDto.setProgramacionList(null);
				personaDto.setUsuarioDto(null);
				listaEmpleados.add(personaDto);
			}
			return listaEmpleados;
		} catch (Exception e) {
			Logger.getLogger(PersonaSeviceIpml.class.getName()).log(Logger.Level.ERROR, e);
			throw new Exception("Error inesperado");
		}
	}

	@Override
	public List<PersonaDto> listaTecnicos() throws Exception {
		try {
			List<PersonaDto> listaClientes = new ArrayList();
			for (Persona persona : personaRepositoryDao.findAll()) {
				if (persona.getCargo().contains("tecnico")) {
					listaClientes.add(new PersonaDto(persona));
				}
			}
			return listaClientes;
		} catch (Exception e) {
			Logger.getLogger(PersonaSeviceIpml.class.getName()).log(Logger.Level.ERROR, e);
			throw new Exception("Error inesperado");
		}
	}

	@Override
	public boolean crearClienteSuscripcion(PersonaDto personaDto) throws Exception {
		try {
			/* creat persona ------------------------------------------------- */
			persona = convertirDtoEntity(personaDto);
			persona.setCargo(ConstantesServitv.CARGO_CLIENTE);
			persona.setTipoDocumento(new TipoDocumento(personaDto.getIdTipo()));
			persona.setIdSucursal(new Sucursal(personaDto.getIdSucursal()));
			/* creat suscripcion --------------------------------------------- */
			suscripcionDto = new SuscripcionDto();
			suscripcionDto.setFecha(new Date());
			suscripcionDto.setValorTotal(suscripcionDto.getValorTotal());
			/* crear suscripcion --------------------------------------------- */
			servicioDto = new ServicioDto();
			servicioDto.setEstadoServ("pendiente");
			/* salvar registros */
			personaDto.setIdPersona(personaRepositoryDao.save(persona).getIdPersona());
			suscripcionDto.setIdPersonaDto(personaDto);
			int idSus = suscripcionServiceImpl.crearSuscripcionCliente(suscripcionDto);
			servicioDto.setIdServicio(idSus);
			servicioServiceImpl.crearClienteServicio(servicioDto, personaDto.getServiSuscripcion());

			return true;
		} catch (Exception e) {
			System.out.println("Error" + e);
			if (e.getMessage().contains("constraint [numero_documento_UNIQUE]")) {
				throw new Exception("Error numero documento duplícado");
			} else {
				Logger.getLogger(PersonaSeviceIpml.class.getName()).log(Logger.Level.ERROR, e);
				throw new Exception("Error inesperado");
			}
		}
	}

	@Override
	public PersonaDto busquedaCliente(String cedula) throws Exception {
		List<FacturaDto> listaFactura = new ArrayList();
		try {
			persona = new Persona();
			persona = personaRepositoryDao.findByCliente(cedula, ConstantesServitv.CARGO_CLIENTE);
			persona.getSuscripcion().getCuenta().getFacturaList()
					.forEach(fact -> listaFactura.add(new FacturaDto(fact)));
			personaDto = new PersonaDto(persona);
			personaDto.getCuentaDto().setFacturaList(listaFactura);
			return personaDto;
		} catch (Exception e) {
			Logger.getLogger(PersonaSeviceIpml.class.getName()).log(Logger.Level.ERROR, e);
			throw new Exception("Error inesperado");
		}
	}

	/**
	 *
	 * @return @throws Exception
	 */
	@Override
	public List<PersonaDto> facturasMoraClientes() throws Exception {
		try {
			List<PersonaDto> personas = new ArrayList();
			List<Persona> personasEntity = new ArrayList();

			personasEntity = personaRepositoryDao.findByClienteFacturaMora(ConstantesServitv.ESTADO_MORA);

			for (Persona per : personasEntity) {
				personaDto = new PersonaDto(per);
				personaDto.setCuentaDto(new CuentaDto(per.getSuscripcion().getCuenta()));
				List<FacturaDto> listaFact = new ArrayList();
				for (Factura fac : per.getSuscripcion().getCuenta().getFacturaList()) {
					if (fac.getEstado().equals(ConstantesServitv.ESTADO_MORA)) {
						listaFact.add(new FacturaDto(fac));
					}
				}
				personaDto.getCuentaDto().setFacturaList(listaFact);
				personas.add(personaDto);
			}

			return personas;
		} catch (Exception e) {
			Logger.getLogger(PersonaSeviceIpml.class.getName()).log(Logger.Level.ERROR, e);
			throw new Exception("Error inesperado");
		}
	}

	public boolean subirMigracion(List<PersonaDto> listPersona) throws Exception {

		for (PersonaDto personaDto : listPersona) {
			try {
				personaDto.setIdSucursal(3);//sucursal por default
				
				persona = convertirDtoEntity(personaDto);				
				persona.setCargo(ConstantesServitv.CARGO_CLIENTE);
				persona.setTipoDocumento(new TipoDocumento(personaDto.getIdTipo()));
				persona.setIdSucursal(new Sucursal(personaDto.getIdSucursal()));
				personaRepositoryDao.save(persona);
			} catch (Exception e) {
				System.out.println("Error" + e);
				if (e.getMessage().contains("constraint [numero_documento_UNIQUE]")) {
					throw new Exception("Error numero documento duplícado");
				} else {
					Logger.getLogger(PersonaSeviceIpml.class.getName()).log(Logger.Level.ERROR, e);
					throw new Exception("Error inesperado");
				}
			}
		}
		return true;
	}

	private Persona convertirDtoEntity(PersonaDto personaDto) {
		persona = new Persona();
		persona.setIdPersona(personaDto.getIdPersona());
		persona.setNumeroDocumento(personaDto.getNumeroDocumento());
		persona.setNombre(personaDto.getNombre());
		persona.setApellido(personaDto.getApellido());
		persona.setDireccion(personaDto.getDireccion());
		persona.setTelefono(personaDto.getTelefono());
		persona.setCargo(personaDto.getCargo());
		persona.setEmail(personaDto.getEmail());
		return persona;
	}

}
