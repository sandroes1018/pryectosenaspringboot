/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.SuscripcionDto;
import com.servitv.entity.Persona;
import com.servitv.entity.Sucursal;
import com.servitv.entity.Suscripcion;
import com.servitv.newpackage.repository.dao.PersonaRepositoryDao;
import com.servitv.newpackage.repository.dao.SuscripcionRepositoryDao;
import com.servitv.service.interfaz.SuscripcionService;
import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public class SuscripcionServiceImpl implements SuscripcionService {

    private Suscripcion suscripcion;
    @Autowired
    private SuscripcionRepositoryDao suscripcionRepositoryDao;
    @Autowired
    private PersonaRepositoryDao personaRepositoryDao;

    @Override
    public List<SuscripcionDto> listaSuscripciones() throws Exception {
        try {
            List<SuscripcionDto> listaSuscripcionDto = new ArrayList();
            for (Suscripcion supscripcion : suscripcionRepositoryDao.findAll()) {
                listaSuscripcionDto.add(new SuscripcionDto(suscripcion));
            }
            return listaSuscripcionDto;
        } catch (Exception e) {
            Logger.getLogger(SuscripcionServiceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
    }

    public int crearSuscripcionCliente(SuscripcionDto suscripcionDto)
            throws Exception {
        try {
            suscripcion = convetirDaoEntity(suscripcionDto);
            suscripcion.setValorTotal(30);
            suscripcion.setIdPersona(new Persona(
                    suscripcionDto.getIdPersonaDto().getIdPersona()));

            return suscripcionRepositoryDao.save(suscripcion).getIdSuscripcion();

        } catch (Exception e) {
            Logger.getLogger(SuscripcionServiceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
    }

    private Suscripcion convetirDaoEntity(SuscripcionDto suscripcionDto) {
        suscripcion = new Suscripcion();
        suscripcion.setIdSuscripcion(suscripcionDto.getIdSuscripcion());
        suscripcion.setValorTotal(suscripcionDto.getValorTotal());
        suscripcion.setFecha(suscripcionDto.getFecha());

        return suscripcion;

    }

    @Override
    public boolean crearSuscripcion(SuscripcionDto suscripcionDto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public boolean crearSuscripcionMigracion(List<SuscripcionDto> listSuscripcionDto) throws Exception {
		// TODO Auto-generated method stub
		
		for (SuscripcionDto suscripcionDto : listSuscripcionDto) {
			  try {
		            suscripcion = convetirDaoEntity(suscripcionDto);
		            
		            suscripcion.setIdPersona(personaRepositoryDao.findUsuarioCliente(suscripcionDto.getIdPersonaDto().getNumeroDocumento()));

		            suscripcionRepositoryDao.save(suscripcion);

		        } catch (Exception e) {
		            Logger.getLogger(SuscripcionServiceImpl.class.getName())
		                    .log(Logger.Level.ERROR, e);
		            throw new Exception("Error inesperado");
		        }
		}
		
		return true;
	}
}
