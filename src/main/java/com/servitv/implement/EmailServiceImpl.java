/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.EmailDto;
import com.servitv.service.interfaz.EmailService;
import com.servitv.util.ConstantesServitv;
import java.util.Date;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 *
 * @author jlagosmu
 */
@Component
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Override
    public boolean sendMail(EmailDto emailDto)
            throws Exception {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setText(emailDto.getMensaje());
            message.setTo(emailDto.getEmailDestino());
            message.setSubject(emailDto.getAsunto());
            message.setSentDate(new Date());
            message.setFrom(ConstantesServitv.EMAIL_ORIEGEN);
            mailSender.send(message);
            return true;
        } catch (MailException e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception(ConstantesServitv.ERROR_ENV_CORREO);
        } catch (Exception e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
    }

}
