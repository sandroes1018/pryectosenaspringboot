/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.EmailDto;
import com.servitv.dto.PersonaDto;
import com.servitv.dto.UsuarioDto;
import com.servitv.entity.Persona;
import com.servitv.entity.Rol;
import com.servitv.entity.RolPersona;
import com.servitv.entity.Usuario;
import com.servitv.newpackage.repository.dao.PersonaRepositoryDao;
import com.servitv.newpackage.repository.dao.RolPersonaRepositoryDao;
import com.servitv.newpackage.repository.dao.RolRepositoryDao;
import com.servitv.newpackage.repository.dao.UsuarioRepositoryDao;
import com.servitv.service.interfaz.UsuarioService;
import com.servitv.util.ConstantesServitv;
import com.servitv.util.MessageUtilReturn;
import java.util.Map;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.WebRequest;

/**
 *
 * @author jlagosmu
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

    private Usuario usuario;
    private UsuarioDto usuarioDto;
    private EmailDto emailDto;
    private RolPersona rolPersona;
    private Persona persona;
    @Autowired
    private UsuarioRepositoryDao usuarioRepositoryDao;
    @Autowired
    private PersonaRepositoryDao personaRepositoryDao;
    @Autowired
    private EmailServiceImpl emailServiceImpl;
    @Autowired
    private RolPersonaRepositoryDao rolPersonaRepositoryDao;
    @Autowired
    RolRepositoryDao rolRepositoryDao;

    /**
     *
     * @param usuarioDto
     * @return
     * @throws Exception
     */
    @Override
    public boolean crearUsuarioAdmin(UsuarioDto usuarioDto) throws Exception {
        persona = new Persona();
        rolPersona = new RolPersona();
        emailDto = new EmailDto();
        try {
            persona = personaRepositoryDao.findById(usuarioDto.getIdPersona()).get();
            usuario = convertirDtoEntidad(usuarioDto);
            usuario.setIdpersona(persona);
            usuario.setEstado(ConstantesServitv.ESTADO_ACTIVO);
            rolPersona.setIdRol(new Rol(usuarioDto.getRol()));
            rolPersona.setIdUsuario(new Usuario(usuarioRepositoryDao
                    .save(usuario).getIdUsuario()));
            rolPersonaRepositoryDao.save(rolPersona);
            /**
             * * confirmacion por correo **********
             */
            emailDto.setEmailDestino(persona.getEmail());
            emailDto.setAsunto(ConstantesServitv.ASUNTO_EMAIL_CREACION_USUARIO);
            emailDto.setUsuario(usuarioDto.getNombre());
            emailDto.setMensaje(ConstantesServitv.MSN_EMAIL_CEACION_USER
                    + MessageUtilReturn.datosUsuarioMensajeEmail(
                            usuario, new PersonaDto(persona)));

            emailServiceImpl.sendMail(emailDto);
            return true;
        } catch (MailException e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error al enviar el correo");
        } catch (Exception e) {

            if (e.getMessage().contains(ConstantesServitv.ERROR_ENV_CORREO)) {
                throw new Exception(e.getMessage());
            } else {
                Logger.getLogger(PersonaSeviceIpml.class.getName())
                        .log(Logger.Level.ERROR, e);
                throw new Exception("Error inesperado");
            }

        }
    }

    /**
     *
     * @param usuarioDto
     * @return
     * @throws Exception
     */
    @Override
    public boolean crearUsuarioCliente(UsuarioDto usuarioDto) throws Exception {
        persona = new Persona();
        rolPersona = new RolPersona();
        emailDto = new EmailDto();
        Rol rol = new Rol();
        try {
            persona = personaRepositoryDao
                    .findUsuarioCliente(usuarioDto.getNumeroDocumento());

            String estado = persona.getSuscripcion().getCuenta().getEstadoCuen();
            if (persona != null && estado.equals("activo")) {
                usuario = convertirDtoEntidad(usuarioDto);
                usuario.setPassword(String.valueOf(persona.getNombre() + Math.random()));
                usuario.setIdpersona(persona);
                usuario.setEstado(ConstantesServitv.ESTADO_ACTIVO);
                rol = rolRepositoryDao.findByNombreRol("cliente");
                rolPersona.setIdRol(rol);
                rolPersona.setIdUsuario(new Usuario(usuarioRepositoryDao
                        .save(usuario).getIdUsuario()));
                rolPersonaRepositoryDao.save(rolPersona);
                /**
                 * * confirmacion por correo **********
                 */
                emailDto.setEmailDestino(persona.getEmail());
                emailDto.setAsunto(ConstantesServitv.ASUNTO_EMAIL_CREACION_USUARIO);
                emailDto.setUsuario(usuarioDto.getNombre());
                emailDto.setMensaje(ConstantesServitv.MSN_EMAIL_CEACION_USER
                        + MessageUtilReturn.datosUsuarioMensajeEmail(
                                usuario, new PersonaDto(persona)));
                emailServiceImpl.sendMail(emailDto);
                return true;
            } else {
                throw new Exception("No se encuentra registrado como cliente activo"
                        + "verifique sus datos");
            }

        } catch (MailException e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception(ConstantesServitv.ERROR_ENV_CORREO);
        } catch (Exception e) {

            if (e.getMessage().contains(ConstantesServitv.ERROR_ENV_CORREO)) {
                throw new Exception(e.getMessage());
            } else {
                Logger.getLogger(PersonaSeviceIpml.class.getName())
                        .log(Logger.Level.ERROR, e);
                throw new Exception("Error inesperado");
            }

        }
    }

    /**
     *
     * @param usuarioDto
     * @return
     */
    public Usuario convertirDtoEntidad(UsuarioDto usuarioDto) {
        usuario = new Usuario();
        usuario.setNombre(usuarioDto.getNombre().toLowerCase());
        usuario.setPassword(usuarioDto.getPassword());
        usuario.setEstado(usuarioDto.getEstado());
        usuario.setIdUsuario(usuarioDto.getIdUsuario());
        return usuario;
    }

}
