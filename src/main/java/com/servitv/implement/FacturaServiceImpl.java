/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.implement;

import com.servitv.dto.CuentaDto;
import com.servitv.dto.FacturaDto;
import com.servitv.dto.PersonaDto;
import com.servitv.dto.SuscripcionDto;
import com.servitv.entity.Factura;
import com.servitv.newpackage.repository.dao.FacturaRepositoryDao;
import com.servitv.service.interfaz.FacturaService;
import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public class FacturaServiceImpl implements FacturaService {

    private Factura factura;
    private FacturaDto facturaDto;

    @Autowired
    private FacturaRepositoryDao facturaRepositoryDao;

    @Override
    public List<FacturaDto> facturasCliente(String numeroDocumento) throws Exception {
        List<FacturaDto> lisaFacturasClientes = new ArrayList();
        try {
            facturaRepositoryDao.findFacturaByNumberDocument(numeroDocumento, "cliente")
                    .forEach(fact -> {
                        facturaDto = new FacturaDto(fact);
                        lisaFacturasClientes.add(facturaDto);
                        facturaDto.setCuentaDto(new CuentaDto(fact.getIdCuenta()));
                    }
                    );
            return lisaFacturasClientes;
        } catch (Exception e) {
            Logger.getLogger(ControlServiciosSeviceImpl.class.getName())
                    .log(Logger.Level.ERROR, e);
            throw new Exception("Error inesperado");
        }
    }

    @Override
    public FacturaDto facturaCliente(int idFactura) throws Exception {
        factura = facturaRepositoryDao.findByClienteFactura(idFactura);
        facturaDto = new FacturaDto(factura);
        facturaDto.getCuentaDto().setIdSuscripcion(new SuscripcionDto());
        facturaDto.getCuentaDto().getIdSuscripcion().setIdPersonaDto(
                new PersonaDto(factura.getIdCuenta()
                        .getIdSuscripcion().getIdPersona()));
        return facturaDto;
    }

}
