/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.service.interfaz;

import com.servitv.dto.UsuarioDto;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public interface UsuarioService {

    public boolean crearUsuarioAdmin(UsuarioDto usuarioDto) throws Exception;

    public boolean crearUsuarioCliente(UsuarioDto usuarioDto) throws Exception;
;
}
