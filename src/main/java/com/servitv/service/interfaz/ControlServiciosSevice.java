/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.service.interfaz;

import com.servitv.dto.TipoServicioDto;
import java.util.List;
import org.springframework.stereotype.Service;


/**
 *
 * @author jlagosmu
 */
@Service
public interface ControlServiciosSevice {

    public void crearServicio(TipoServicioDto tipoServicioDto);

    public List<TipoServicioDto> listaTipoSerivicios() throws Exception;

    public TipoServicioDto buscarSerivicio(int idTipoServicio) throws Exception;
    
    public boolean eliminarTipoSrvicio(int idTipoServicio) throws Exception;

    public List<TipoServicioDto> listaTipoSeriviciosInternet() throws Exception ;
    
    public List<TipoServicioDto> listaTipoSeriviciosTv() throws Exception ;
}
