package com.servitv.service.interfaz;

import java.util.List;

import org.springframework.stereotype.Service;

import com.servitv.dto.MaterialDto;
import com.servitv.dto.ProgramacionDto;
import com.servitv.entity.Programacion;




@Service
public interface ProgramacionService {

	public boolean newProgramacion(MaterialDto pr) throws Exception;
	
	public List<ProgramacionDto> getProgramacion() throws Exception;
	
	public ProgramacionDto getProgramacionId(int id) throws Exception;

}
