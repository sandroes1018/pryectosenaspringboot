/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.service.interfaz;

import com.servitv.dto.EmailDto;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public interface EmailService {

    /**
     *
     * @param emailDto
     * @param mensaje
     * @param usuario
     * @return
     * @throws Exception
     */
    public boolean sendMail(EmailDto emailDto) 
            throws Exception;
}
