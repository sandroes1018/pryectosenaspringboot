/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.service.interfaz;

import com.servitv.dto.SuscripcionDto;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public interface SuscripcionService {
    
    
   public List<SuscripcionDto> listaSuscripciones() throws Exception;
   
   public boolean crearSuscripcion(SuscripcionDto suscripcionDto) throws Exception;
   
   public boolean crearSuscripcionMigracion(List<SuscripcionDto> listSuscripcionDto) throws Exception;
   
   
    
}
