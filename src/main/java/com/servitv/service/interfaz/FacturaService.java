/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.service.interfaz;

import com.servitv.dto.FacturaDto;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public interface FacturaService {

    public List<FacturaDto> facturasCliente(String documento)throws Exception;

    public FacturaDto facturaCliente(int idFactura)throws Exception;

}
