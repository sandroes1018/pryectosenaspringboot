/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.service.interfaz;

import com.servitv.dto.PersonaDto;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */
@Service
public interface PersonaSevice {

    public List<PersonaDto> facturasMoraClientes()throws Exception;
    
    public List<PersonaDto> listaClientes() throws Exception;
    
    public List<PersonaDto> listaEmpleado() throws Exception;
    
    public List<PersonaDto> listaTecnicos() throws Exception;
    
    public boolean crearClienteSuscripcion(PersonaDto personaDto) throws Exception;
    
    public boolean subirMigracion(List<PersonaDto> listPersona) throws Exception;

    public PersonaDto busquedaCliente(String cedula) throws Exception;
}
