/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.service.interfaz;

import com.servitv.dto.TipoDocumentoDto;
import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 *
 * @author jlagosmu
 */

@Service
public interface TipoDocumentoService {
    
    /**
     *
     * @return
     * @throws Exception
     */
    public List<TipoDocumentoDto> listaTipoDocumento() throws Exception;
}
