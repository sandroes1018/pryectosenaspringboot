/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.util;

/**
 *
 * @author jlagosmu
 */
public class ConstantesServitv {

    public static final String ESTADO_PENDIENTE = "pendiente";
    public static final String ESTADO_PAGADA = "pagada";
    public static final String ESTADO_CANCELADA = "cancelada";
    public static final String ESTADO_ABONADA = "abonada";
    public static final String ESTADO_SOBRE_PAGDADA = "sobre pagada";
    public static final String ESTADO_MORA = "mora";
    public static final int ESTADO_INACTIVO = 0;
    public static final int ESTADO_ACTIVO = 1;
    public static final String ESTADO_ACTIVO_STRING = "1";
    public static final String CLIENTE = "cliente";
    public static final String RUTA_CLIENTE = "CLI";
    public static final String RUTA_ADMIN = "AMD";
    public static final String EMAIL_ORIEGEN = "emalredtv@gmail.com";
    public static final String CARGO_CLIENTE = "cliente";
    public static final String ROL_CLIENTE = "cliente";
    public static final String ERROR_ENV_CORREO = "Error al enviar el correo"; 
    public static final String ERROR_LOGIN = "error de usuario o contraseña";


    /* constaantes email*/
    public static final String ASUNTO_EMAIL_CREACION_USUARIO = "creacion de usuario";
    public static final String MSN_EMAIL_CEACION_USER = "Bienvenido "
            + "ha sido generada su nueva cuenta en el aplicativo Servitv";

}
