/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.util;

import com.servitv.dto.PersonaDto;
import com.servitv.dto.UsuarioDto;
import com.servitv.entity.Usuario;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author jlagosmu
 */
public class MessageUtilReturn {

    public static HashMap responseMessage(String clave, String valor) {
        HashMap<String, String> response = new HashMap();
        response.put(clave, valor);
        return response;
    }

    public static String datosUsuarioMensajeEmail(Usuario usuario, PersonaDto personaDto) {
        String mensaje = "";
        mensaje = "\n"
                + "- Nombre :   " + personaDto.getNombre() + " " + personaDto.getApellido() + "\n"
                + "- Numero Documento  " + personaDto.getNumeroDocumento() + "\n"
                + "- Nombre usuario :  " + usuario.getNombre() + "\n"
                + "- contraseña :  " + usuario.getPassword() + "\n"
                + "fecha:  "+new Date();
        

        return mensaje;
    }

}
