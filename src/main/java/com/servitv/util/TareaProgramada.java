package com.servitv.util;


import java.util.List;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.servitv.dto.EmailDto;
import com.servitv.entity.Cuenta;

import com.servitv.implement.EmailServiceImpl;
import com.servitv.newpackage.repository.dao.CuentaRepositoryDao;
import com.servitv.service.interfaz.EmailService;


@SpringBootApplication
@EnableScheduling
public class TareaProgramada {
	@Autowired
	private CuentaRepositoryDao cuentaRepositoryDao;
	@Autowired
    private EmailService emailService;
	
	public TareaProgramada() {
		// TODO Auto-generated constructor stub
	}
	
	//@Scheduled(cron = "0 0 8 20 * ? ")
	@Scheduled(cron = "0 19 19 ? * ? ")	 
	public void SendAlertFacturas() throws Exception {
		
		List<Cuenta> listEmail=  cuentaRepositoryDao.findEstadoCuen(ConstantesServitv.ESTADO_ACTIVO_STRING);		
				
		for (Cuenta cuenta : listEmail) {		
			System.out.println("/************/ Correo "+cuenta.getIdSuscripcion().getIdPersona().getEmail());
			EmailDto email= new EmailDto();
			email.setAsunto("Nueva Factura");
			email.setMensaje("Buen dia "+cuenta.getIdSuscripcion().getIdPersona().getNombre()+", este correo es para informarle que se ha generado una nueva factura.");
			email.setEmailDestino(cuenta.getIdSuscripcion().getIdPersona().getEmail());
			//notificationService.sendNotificaitoin(email);
			emailService.sendMail(email);						
			System.out.println("Correo Enviado a usuario");
			
		}
		
	}
	
	
	
	

}
