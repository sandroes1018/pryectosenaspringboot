package com.servitv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
//@ComponentScan({"com.servitv.service.interfaz"})
public class ServitvApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServitvApplication.class, args);
    }    
  
}
