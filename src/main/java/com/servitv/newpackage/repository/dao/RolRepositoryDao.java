/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.newpackage.repository.dao;

import com.servitv.entity.Rol;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jlagosmu
 */
@Repository
public interface RolRepositoryDao extends  JpaRepository<Rol, Serializable>{
    
    public Rol findByNombreRol(String nombre);
}
