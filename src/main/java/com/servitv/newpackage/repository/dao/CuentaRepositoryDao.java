/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.newpackage.repository.dao;

import com.servitv.entity.Cuenta;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jlagosmu
 */
@Repository
public interface CuentaRepositoryDao extends JpaRepository <Cuenta,Integer> {
    
	public List<Cuenta> findEstadoCuen(@Param("estadoCuen") String estadoCuen);	
	
}
