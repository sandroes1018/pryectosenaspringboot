/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.newpackage.repository.dao;
import com.servitv.entity.TipoServicio;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author jlagosmu
 */
@Repository
public interface ControlServicioRepositoryDao extends JpaRepository<TipoServicio, Integer> {
    
}
