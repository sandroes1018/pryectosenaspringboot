/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.newpackage.repository.dao;

import com.servitv.entity.Persona;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jlagosmu
 */
public interface PersonaRepositoryDao extends JpaRepository<Persona, Integer> {
    
   
    public Persona findByCliente(String numeroDocumento,String cargo);
    
    public List<Persona> findByClienteFacturaMora(String estado); 
    public List<Persona> findEmpleados(@Param("cargo") String cargo);
    public Persona findUsuarioCliente(@Param("numeroDocumento") String numeroDocumento);
    
}
