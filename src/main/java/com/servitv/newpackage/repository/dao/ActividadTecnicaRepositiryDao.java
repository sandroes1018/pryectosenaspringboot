package com.servitv.newpackage.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.servitv.entity.ActividadTecnica;

@Repository
public interface ActividadTecnicaRepositiryDao extends JpaRepository<ActividadTecnica, Integer> {

}
