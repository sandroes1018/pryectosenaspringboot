/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.newpackage.repository.dao;

import com.servitv.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jlagosmu
 */
@Repository
public interface UsuarioRepositoryDao extends JpaRepository< Usuario, Integer> {

    public Usuario findUsuarioPassword(@Param("nombre") String nombre,@Param("password") String password, @Param("estado") int estado);
}
