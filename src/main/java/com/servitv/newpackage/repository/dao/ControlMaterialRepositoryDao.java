package com.servitv.newpackage.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.servitv.entity.Material;

@Repository
public interface ControlMaterialRepositoryDao extends JpaRepository<Material, Integer> {

}
