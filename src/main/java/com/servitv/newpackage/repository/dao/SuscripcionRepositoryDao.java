/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.newpackage.repository.dao;

import com.servitv.entity.Suscripcion;

import org.junit.runners.Parameterized.Parameters;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jlagosmu
 */
@Repository
public interface SuscripcionRepositoryDao extends JpaRepository<Suscripcion, Integer>{
    
	public Suscripcion findByIdCliente(@Param("idPersona") int idPersona);
}
