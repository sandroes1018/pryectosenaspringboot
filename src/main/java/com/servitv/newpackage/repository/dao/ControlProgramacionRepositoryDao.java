package com.servitv.newpackage.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.servitv.entity.Programacion;

@Repository
public interface ControlProgramacionRepositoryDao extends JpaRepository<Programacion, Integer> {

}
