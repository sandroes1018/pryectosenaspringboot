/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.newpackage.repository.dao;
import com.servitv.entity.TipoDocumento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author jlagosmu
 */
@Repository
public interface TipoDocumentoRepositoryDao extends JpaRepository<TipoDocumento, Integer> {
    
}
