/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.newpackage.repository.dao;

import com.servitv.entity.Factura;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jlagosmu
 */
@Repository
public interface FacturaRepositoryDao extends JpaRepository<Factura, Integer>{
 
    public List<Factura> findFacturaByNumberDocument(String numeroDocumento,String cargo);
    
    public Factura findByClienteFactura(int idFactura);
    
}
