/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.controller;

import com.servitv.dto.TipoDocumentoDto;
import com.servitv.service.interfaz.TipoDocumentoService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jlagosmu
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/td")
public class TipoDocumentoController {
    
    @Autowired
    private TipoDocumentoService tipoDocumentoService;

    public TipoDocumentoService getTipoDocumentoService() {
        return tipoDocumentoService;
    }

    public void setTipoDocumentoService(TipoDocumentoService tipoDocumentoService) {
        this.tipoDocumentoService = tipoDocumentoService;
    }

    public TipoDocumentoController() {
       
    }

    @GetMapping(value = "/tipoDocumento")
    public List<TipoDocumentoDto> listaTipoSerivicios() throws Exception {
        return tipoDocumentoService.listaTipoDocumento();
    }
    
}
