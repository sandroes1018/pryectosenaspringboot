/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.controller;

import com.servitv.dto.FacturaDto;
import com.servitv.service.interfaz.FacturaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jlagosmu
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/fac")
public class FacturaController {
    
    @Autowired
    private FacturaService facturaService;
    
    @GetMapping("/factura/{id}")
    public List<FacturaDto> facturasCliente(@PathVariable("id") String documento)
            throws Exception {
        return facturaService.facturasCliente(documento);
    }
    
     @GetMapping("/factura/und/{id}")
    public FacturaDto facturaCliente(@PathVariable("id") int idFactura)
            throws Exception {
        return facturaService.facturaCliente(idFactura);
    }
    

}
