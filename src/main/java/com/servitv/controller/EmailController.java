/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.controller;

import com.servitv.dto.EmailDto;
import com.servitv.service.interfaz.EmailService;
import com.servitv.util.MessageUtilReturn;
import java.util.HashMap;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jlagosmu
 */
@RestController
@RequestMapping("/servitv")
public class EmailController {

    @Autowired
    private EmailService emailService;

    @PutMapping("/email")
    public HashMap sendMail(@RequestBody @Valid EmailDto emailDto)
            throws Exception {
        emailService.sendMail(emailDto);
        return MessageUtilReturn.responseMessage("message", "Correo enviado correctamente");
    }    
 }
