/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.controller;

import com.servitv.dto.UsuarioDto;
import com.servitv.service.interfaz.UsuarioService;
import com.servitv.util.MessageUtilReturn;
import java.util.HashMap;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jlagosmu
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/us/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PutMapping(value="/admAplicacion")
    public HashMap crearUsuarioAdmin(@RequestBody @Valid UsuarioDto usuarioDto)
            throws Exception {
        usuarioService.crearUsuarioAdmin(usuarioDto);
        return MessageUtilReturn
                .responseMessage("messenger", "usuario creado correctamente");
    }
    
    @PutMapping(value="/cliAplicacion")
    public HashMap crearUsuarioCliente(@RequestBody @Valid UsuarioDto usuarioDto)
            throws Exception {
        usuarioService.crearUsuarioCliente(usuarioDto);
        return MessageUtilReturn
                .responseMessage("messenger", "usuario creado correctamente"
                        + " su contraseña fue enviada a su correo electronico");
    }
}
