/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.controller;

import com.servitv.dto.SuscripcionDto;
import com.servitv.service.interfaz.SuscripcionService;
import com.servitv.util.MessageUtilReturn;

import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jlagosmu
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/sus")
public class SuscripcionController {
    
    @Autowired
    private SuscripcionService suscripcionService;
    
    
    
    @GetMapping(value = "/suscripcion")
    public List<SuscripcionDto> listaSuscripciones() throws Exception {
        return suscripcionService.listaSuscripciones();
    }

    @PutMapping("/suscripcion")
    public boolean crearSuscripcion(@RequestBody @Valid SuscripcionDto suscripcionDto) throws Exception {
        suscripcionService.crearSuscripcion(suscripcionDto);

        return true;
    }
    
    @PutMapping("/suscripcion/migracion")
    public HashMap crearSuscripcionMigracion(@RequestBody @Valid List<SuscripcionDto> suscripcionDto) throws Exception {
    	
        suscripcionService.crearSuscripcionMigracion(suscripcionDto);

        return MessageUtilReturn.responseMessage("msg", "Registros ingresados "+suscripcionDto.size());
    }
}
