/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.controller;

import com.servitv.dto.UsuarioDto;
import com.servitv.service.interfaz.LoginService;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jlagosmu
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class LonginController {

    @Autowired
    private LoginService loginService;

    public LonginController() {
    }

//    @GetMapping("/login/token")
//    public UsuarioDto loginUser()  throws Exception{
//       return usuarioDto;
//    }
    @PostMapping("/login")
    public HashMap loginUser(@RequestBody UsuarioDto user) throws Exception {
        return loginService.loginUser(user);
    }

}
