/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.controller;

import com.servitv.dto.PersonaDto;
import com.servitv.service.interfaz.PersonaSevice;
import com.servitv.util.MessageUtilReturn;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jlagosmu
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/per")
public class PersonaController {

    
    @Autowired
    private PersonaSevice personaSevice;

    @GetMapping(value = "/persona/clientes")
    public List<PersonaDto> listaClientes() throws Exception {
        return personaSevice.listaEmpleado();
    }
    
    @GetMapping(value = "/persona/empleado")
    public List<PersonaDto> listaEmpleado() throws Exception {
        return personaSevice.listaEmpleado();
    }

    @PutMapping("/persona/suscripcion")
    public HashMap crearClienteSuscripcion(@RequestBody @Valid PersonaDto personaDto)
            throws Exception {
        if (personaDto.getServiSuscripcion().length <= 0) {
            throw new Exception("Debe eligir un servicio");
        } else {
           personaSevice.crearClienteSuscripcion(personaDto);
        }
        return MessageUtilReturn
                .responseMessage("message", "Suscripcion Generada Coreectamente");

    }
    
    @PutMapping("persona/migracion")
    public HashMap crearPersonaMigracion(@RequestBody @Valid List<PersonaDto> list) throws Exception {
    	
    	personaSevice.subirMigracion(list);
    	
    	return MessageUtilReturn.responseMessage("message", "Migracion Generada");
    }

    @GetMapping("/persona/tecnicos")
    public List<PersonaDto> listaTenicos() throws Exception {
        return personaSevice.listaTecnicos();
    }

    @GetMapping("/persona/cliente/{cc}")
    public PersonaDto busquedaCliente(@PathVariable("cc") String documento)
            throws Exception {
        return personaSevice.busquedaCliente(documento);
    }
    
    @GetMapping("/persona/clientes/mora")
    public List<PersonaDto> facturasMoraClientes() throws Exception{
        return personaSevice.facturasMoraClientes();
    }

}
