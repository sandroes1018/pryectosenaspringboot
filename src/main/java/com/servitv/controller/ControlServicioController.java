/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.controller;

import com.servitv.dto.TipoServicioDto;
import com.servitv.service.interfaz.ControlServiciosSevice;
import com.servitv.util.MessageUtilReturn;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jlagosmu
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/cs")
public class ControlServicioController {

    @Autowired
    private ControlServiciosSevice controlServiciosSevice;

    public ControlServiciosSevice getControlServiciosSevice() {
        return controlServiciosSevice;
    }

    public void setControlServiciosSevice(ControlServiciosSevice controlServiciosSevice) {
        this.controlServiciosSevice = controlServiciosSevice;
    }

    @GetMapping(value = "/controlServicio")
    public List<TipoServicioDto> listaTipoSerivicios() throws Exception {
        return controlServiciosSevice.listaTipoSerivicios();
    }

    @PutMapping("/controlServicio")
    public HashMap crearTiposervicio(@RequestBody @Valid TipoServicioDto tipoServicioDto) {
        controlServiciosSevice.crearServicio(tipoServicioDto);
        return MessageUtilReturn.responseMessage(
                "message", "Servicio Creado Correctamente");
    }

    @GetMapping("/controlServicio/tipo/{id}")
    public TipoServicioDto buscarSerivicio(@PathVariable("id") int idTipoServicio)
            throws Exception {
        return controlServiciosSevice.buscarSerivicio(idTipoServicio);
    }

    @DeleteMapping("/controlServicio/tipo/{id}")
    public HashMap eliminarTipoSrvicio(@PathVariable("id") int idTipoServicio)
            throws Exception {
        controlServiciosSevice.eliminarTipoSrvicio(idTipoServicio);
        return MessageUtilReturn.responseMessage(
                "message", "Servicio Eliminado Correctamente");
    }

    @GetMapping(value = "/controlServicioInt")
    public List<TipoServicioDto> listaTipoSeriviciosInternet() throws Exception {
        return controlServiciosSevice.listaTipoSeriviciosInternet();
    }

    @GetMapping(value = "/controlServicioTv")
    public List<TipoServicioDto> listaTipoSeriviciosTv() throws Exception {
        return controlServiciosSevice.listaTipoSeriviciosTv();
    }
}
