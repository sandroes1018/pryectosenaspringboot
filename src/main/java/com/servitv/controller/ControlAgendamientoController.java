package com.servitv.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.servitv.dto.ActividadtecnicaDto;
import com.servitv.dto.MaterialDto;
import com.servitv.dto.ProgramacionDto;
import com.servitv.dto.TipoServicioDto;
import com.servitv.entity.Material;
import com.servitv.entity.Programacion;
import com.servitv.service.interfaz.ControlServiciosSevice;
import com.servitv.service.interfaz.ProgramacionService;

@RestController
@RequestMapping("/ca")
public class ControlAgendamientoController {
	
	 @Autowired
	  private ProgramacionService programacionService;
	
	public ControlAgendamientoController() {
		// TODO Auto-generated constructor stub
	}
	
		@PutMapping("/newAgendamiento")
	    public boolean crearProgramacion(@RequestBody @Valid MaterialDto newProgramacion) throws Exception {
			
		//System.out.println(newProgramacion.getProgramacion().getCuenta().getIdCuenta());
			
			return	programacionService.newProgramacion(newProgramacion);
			
	    }
		
		@GetMapping("/getAgendamientos")
	    public List<ProgramacionDto> GetProgramacion() throws Exception {
			
			//System.out.println(newProgramacion.getProgramacion().getCuenta().getIdCuenta());			
			return programacionService.getProgramacion();
			
	    }
		
		 @GetMapping("/getAgendamientos/{id}")
		    public ProgramacionDto buscarSerivicio(@PathVariable("id") int id) throws Exception {
		            
		        return programacionService.getProgramacionId(id);
		    }
		
		@GetMapping("/example")
	    public MaterialDto example() {
				
			MaterialDto m = new  MaterialDto();
			
			m.setCantidad(1);
			m.setDescripcion("Desc");
		
			return m;
	    }
		
		
		

}
