/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.controller;

import com.servitv.dto.PagoDto;
import com.servitv.service.interfaz.PagoSevice;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author jlagosmu
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/pag")
public class PagoController {
    
    @Autowired
    private PagoSevice pagoSevice;
    
    
    @GetMapping(value = "/pago")
    public List<PagoDto> listaPagos() throws Exception {
        return pagoSevice.listaPagos();
    }

    @PutMapping("/pago")
    public boolean registroPago(@RequestBody @Valid PagoDto pagoDto) throws Exception {
        pagoSevice.registroPago(pagoDto);
        return true;
    }
}
