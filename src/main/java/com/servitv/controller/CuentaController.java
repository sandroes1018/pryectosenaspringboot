/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servitv.controller;

import com.servitv.dto.CuentaDto;
import com.servitv.service.interfaz.CuentaService;
import com.servitv.util.MessageUtilReturn;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jlagosmu
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/cu")
public class CuentaController {

    @Autowired
    private CuentaService cuentaService;

    @GetMapping(value = "/cuenta")
    public List<CuentaDto> listaSucursales() {
        return cuentaService.listaCuentas();
    }
    @PutMapping(value="/cuenta/migracion")
    public HashMap cuentaMigracion(@RequestBody @Valid List<CuentaDto> list) throws Exception {
    	
    	cuentaService.cuentaMigracion(list);
    	return MessageUtilReturn.responseMessage("msg", "Registros insertados "+list.size());
    }
    
    
}
